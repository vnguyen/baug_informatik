

/**
 * Code from Exercise Session Informatik D-BAUG 10.10.2019
 * Commented on 11.10.2019
 */
public class Main {

    public static void main(String[] args) throws Exception {
        
        // Read the number of trials
        Out.print("Number of trials: ");
        int n = In.readInt();
        
        // Perform the Monte Carlo Simulation in a loop
        for (int i = 1; i <= n; i = i * 2) {
          //Perform the simulation for 1, 2, 4, 8, ..., n points
          
          //save the number of circle hits in current trial
          int hits = 0; 
          
          for (int j = 0; j < i; j++) {
            //repeat i times:
        
            //generate random point
            double x = Math.random();
            double y = Math.random();
            
            //check if point is a hit
            if (x*x + y*y <= 1) {
              //hit => remember the hit by increasing hit counter
              hits++;
            }
          }
          
          /*
          compute pi. The proportion between pi (area of the circle) and 4 (area of the rectangle)
          is the same as the proportion between the number of circle hits and teh total number of points
          */
          double pi = ((double)hits / (double)i) * 4.0;
          
          //Output computed result
          Out.println(i + " trials: Pi = " + pi);
        }
    }
}
