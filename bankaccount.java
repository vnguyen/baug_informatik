class BankAccount {
    private String name;
    private double Kontostand;
    private double interest;

    public BankAccount(String name, double amount) {
        this.name = name;
        this.Kontostand = amount;
        this.interest = 0.005;
    }

    boolean schicken (double amount, BankAccount receiver) {
        if (this.Kontostand >= amount) {
            receiver.zunehmen(amount);
            this.Kontostand -= amount;
            return true;
        } else {
            return false;
        }
    }

    double zunehmen (double amount) {
        this.Kontostand += amount;
        return this.Kontostand;
    }

    double abnehmen (double amount) {
        this.Kontostand -= amount;
        return this.Kontostand;
    }

    public String toString() {
        return "Name: " + this.name + "\nKontostand: " + this.Kontostand;
    }

}

Main:

BankAccount a1 = new BankAccount("Vu", 50);

BankAccount a2 = new BankAccount("Tim", 200);


a2.schicken(20, a1);

BankAccount[] listOfAccounts = new BankAccount[2];
listOfAccounts[0] = a1;
listOfAccounts[1] = a2;

Out.print(a1.toString());