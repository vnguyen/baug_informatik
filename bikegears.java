// Front: 44, 33, 22
// Rear: 11, 13, 15, 17, 19, 21, 24, 28, 32, 36

public class Main {
  public static void main(String[] args) {
    // Read the current front and back gear (number of teeth)
    Out.print("front? ");
    int cur_front = In.readInt();
    Out.print("rear? ");
    int cur_rear = In.readInt();
    
    int front[] = { 44, 33, 22 };
    int rear[] = { 11, 13, 15, 17, 19, 21, 24, 28, 32, 36 };
    
    // Store 'current' closest ratio
    double closest_ratio = 0;
    int closest_front = 0;
    int closest_rear = 0;
    
    // initial ratio
    double cur_ratio = (double)cur_front / cur_rear;
    
    // loop through all possible (front[i], rear[j]) combinations
    for (int i = 0; i < front.length; i++) {
      for (int j = 0; j < rear.length; j++) {
        /* 
        for each combination: 
        compute current ratio and check if it is better than the closest ratio so far.
        */
        double ratio = (double)front[i] / rear[j];
        
        // if the current ratio is better: update closest ratio
        if(ratio > closest_ratio && ratio < cur_ratio) {
          closest_ratio = ratio;
          closest_front = front[i];
          closest_rear = rear[j];
        }
        
      }
    }
    
    Out.println("Closest ratio: " + closest_ratio);
    Out.println(closest_front + "/" + closest_rear);
  }
}