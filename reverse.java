public class Main {

  public static void repeatInput() {
    
  
    
    String input = In.readLine();
    if (input.trim().equals("end")) {
      //base case
      return;
    } else {
      //recursive case: repeat the program  
      Out.println(input);
      repeatInput();
    }
    
  }
  
  public static void reverseInput() {
    String input = In.readLine();
    if (input.trim().equals("end")) {
      //base case  
      return;
    }
    
    //recursive case: print the reverse without the "first" line input (recursively), then print the "first" line
    reverseInput();
    Out.println(input);
    
  }


  // Don't change the code below
  public static void main(String[] args) {
    Out.println(">");
    if (In.readLine().trim().equals("repeat")) {
      repeatInput();
    } else {
      reverseInput();
    }
    Out.println("<");
  }
}