public class Main {
  
  // PRE: n > 0
  // POST: return x^n
  public static double poweri(double x, int n){
    double result = 1;
    while (n > 0) {
      result *= x;
      --n;
    }
    return result;
  }
  
  // PRE: n > 0
  // POST: return x^n
  public static double powerr(double x, int n){
    double result;
    if (n == 1) {
      //base case  
      return x;
    } else if (n % 2 == 0) {
      //recursive case 1: this holds mathematically if and only if n is even. This case leads to less computations.  
      double halfexponent = powerr(x, n / 2);
      return halfexponent * halfexponent;
    } else {
      //recursive case 2: n is odd  
      return x * powerr(x, n-1);
    }
    
  }
  
  // Entry point to the program. Don't change.
  public static void main(String[] args) {
    Out.println("Testing your recursive solution against the iterative approach:\n");
    for (int x = 0; x < 10; x++) {
      for (int n = 1; n < 10; n++) {
        Out.print(x + "^" + n + " = ");
        double resulti = poweri(x, n);
        double resultr = powerr(x, n);
        assert Math.abs(resulti - resultr) < 5 * Math.ulp(resulti) : 
          "Iterative and recursive evaluation doesn't match up: " + 
          resulti + " != " + resultr;
        Out.println(resulti);
      }
    }
    Out.println("Ok");
  }
}