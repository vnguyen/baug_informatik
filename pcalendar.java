public class Main {
  /**
   * @param year   a year greater or equal to 1900
   * @return       whether that year was a leap year
   */
  static boolean isLeapYear(int year) {
    if (year % 4 != 0) {
      return false;
    } else if (year % 100 != 0) {
      return true;
    } else if (year % 400 != 0) {
      return false;
    } else {
      return true;
    }
  }

  /**
   * @param month  a month between 1 and 12
   * @param year   a year greater or equal than 1900
   * @return       number of days in the month of that year
   */
  static int countDaysInMonth(int month, int year) {
    if (month == 1 || month == 3 || month == 5 || month == 7
    || month == 8 || month == 10 || month == 12) {
      return 31;
    } else if (month == 2) {
      if (isLeapYear(year)) {
        return 29;
      } else {
        return 28;
      }
    } else {
      return 30;
    }
    
  }

  /**
   * @param day    a day between 1 and 31
   * @param month  a month between 1 and 12
   * @param year   a year greater or equal than 1900
   * @return       whether the date is valid
   */
  static boolean isValidDate(int day, int month, int year) {
    boolean valid = true;
    if (!(year >= 1900)) {
      valid = false;
    }
    if (!(month >= 1 && month <= 12)) {
      valid = false;
    }
    if (!(day > 0 && day <= countDaysInMonth(month, year))) {
      valid = false;
    }
    
    return valid;
  }

  /**
   * (the given values should represent a valid date)
   * @param day    a day between 1 and 31
   * @param month  a month between 1 and 12
   * @param year   a year greater or equal than 1900
   * @return       number of days between Monday January 1, 1900 and this date
   */
  static int countDays(int day, int month, int year) {
    int sum = 0;
    for (int i = 1900; i < year; i++) {
      if(isLeapYear(i)) {
        sum += 366;
      } else {
        sum += 365;
      }
    }
    
    for (int i = 1; i < month; i++) {
      sum += countDaysInMonth(i, year);
    }
    
    sum += day;
    sum--;
    
    return sum;
  }

  public static void main(String[] args) {
    int day = In.readInt();
    int month = In.readInt();
    int year = In.readInt();
    
    if(!isValidDate(day, month, year)) {
      Out.println("invalid date");
      return;
    }
    
    switch(countDays(day, month, year) % 7) {
      case 0:
        Out.println("monday");
        break;
      case 1:
        Out.println("tuesday");
        break;
      case 2:
        Out.println("wednesday");
        break;
      case 3:
        Out.println("thursday");
        break;
      case 4:
        Out.println("friday");
        break;
      case 5:
        Out.println("saturday");
        break;
      case 6:
        Out.println("sunday");
        break;
    }
  }
}
