import java.util.HashSet;

public class Main {

  public static void main(String[] args) throws Exception {
    HashSet<Integer> s = new HashSet<Integer>();
    
    s.add(1);
    s.add(2);
    s.add(3);

    printPermutations("", s);
  }
  
  public static void printPermutations(String curr, HashSet<Integer> s) {
    if (s.isEmpty()) {
      Out.println(curr);
    } else {
      for (Integer e : s) {
        HashSet<Integer> newSet = (HashSet)s.clone();
        newSet.remove(e);
        printPermutations(curr + " " + e, newSet);
      } 
    }
  }
}
