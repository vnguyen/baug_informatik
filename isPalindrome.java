import java.util.HashSet;

public class Main {

  public static void main(String[] args) throws Exception {    
    Out.println(isPalindrome("a"));
    Out.println(isPalindrome("ab"));
    Out.println(isPalindrome("aba"));
  }
  
  public static boolean isPalindrome(String curr) {
    if (curr.length() <= 1) {
      return true;
    } else {
      return (curr.charAt(0) == curr.charAt(curr.length()-1)) && isPalindrome(curr.substring(1, curr.length() - 2));
    }
  }
}
