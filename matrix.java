/*
Solution created: 24.10.2019
Commented: 28.10.2019
This code is incomplete: The task requires asserts for edge-cases and false inputs.
*/

public class Main {

  public static double[][] matrixMatrixMultiply(double[][] M, double[][] N) {
    assert M != null : "null matrix encountered";
    assert N != null : "null matrix encountered";
    int MM = M.length;    // rows of M
    int NN = N[0].length; // columns of N
    int KK = M[0].length; // columns of M, should == rows of N
    assert KK == N.length : "number columns matrix M differs from number rows matrix N";
    
    double[][] res = new double[MM][NN];
    

    /*
    Iterate through all (i,j) of result
    */
    for (int i = 0; i < MM; i++) {
        for(int j = 0; j < NN; j++) {
            /*
            For each (i,j) in result: perform vector product
            */
            for (int k = 0; k < KK; k++) {
                res[i][j] += M[i][k] * N[k][j];
            }
        }
    }

    return res;
  }
  
  public static double[][] input() {
    int rows = In.readInt();
    int cols = In.readInt();
    assert rows > 0: "forbidden value for rows";
    assert cols > 0: "forbidden value for columns";
    
    double[][] result = new double[rows][cols];
    for (int r = 0; r < rows; ++r){
      for (int c = 0; c < cols; ++c){
        result[r][c] = In.readDouble();
      }
    }
    return result;
  }
  
  public static void output(double[][] M){
    assert M != null : "null matrix encountered";
    int rows = M.length;
    int cols = M[0].length;
    Out.println(rows + " " + cols);
    for (int r = 0; r < rows; ++r){
      for (int c = 0; c < cols; ++c){
        Out.print(M[r][c], 1); Out.print(" ");
      }
      Out.println();
    }
    
  }
  
  public static void main(String[] args) {
    double[][] M = input();
    double[][] N = input();
    double[][] res = matrixMatrixMultiply(M, N); 
    output(res);
  }
}

        
